//
//  Confesion.swift
//  PoliCrush-IOS
//
//  Created by Isaac on 1/28/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import Foundation

class Confesion{
    
    var autor:String = ""
    var confesion:String = ""
    var edad:Int = 0
    var sexo:String = ""
    var fecha:String = ""
    
    init?(data: [String: Any]?) {
        self.autor = data!["autor"] as? String ?? ""
        self.confesion = data!["confesion"] as? String ?? ""
        self.edad = data!["edad"] as? Int ?? 0
        self.sexo = data!["sexo"] as? String ?? ""
        self.fecha = data!["fecha"] as? String ?? ""
    }
}
