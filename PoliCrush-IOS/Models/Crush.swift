//
//  Crush.swift
//  PoliCrush-IOS
//
//  Created by Isaac on 1/31/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import Foundation

class Crush{
    
    var id:String = ""
    var autor:String = ""
    var nombre:String = ""
    var edad:Int = 0
    var sexo:String = ""
    var fecha:String = ""
    var imageURL:String = ""
    var votos:[String] = []
    
    init?(data: [String: Any]?) {
        self.autor = data!["autor"] as? String ?? ""
        self.nombre = data!["nombre"] as? String ?? ""
        self.edad = data!["edad"] as? Int ?? 0
        self.sexo = data!["sexo"] as? String ?? ""
        self.fecha = data!["fecha"] as? String ?? ""
        self.imageURL = data!["imageURL"] as? String ?? ""
        self.votos = data!["votos"] as? [String] ?? []
    }
}
