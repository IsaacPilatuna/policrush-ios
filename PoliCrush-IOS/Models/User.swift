

import Foundation

class User{
    
    var email:String = ""
    var username:String = ""
    var birthDate:String = ""
    var sex:String = ""
    var facultad:String = ""
    
    init?(data: [String: Any]?) {
        self.email = data!["email"] as? String ?? ""
        self.username = data!["username"] as? String ?? ""
        self.birthDate = data!["birthdate"] as? String ?? ""
        self.sex = data!["sex"] as? String ?? ""
        self.facultad = data!["facultad"] as? String ?? ""
    }
}
