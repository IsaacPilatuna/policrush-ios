//
//  SexPickerViewController.swift
//  PoliCrush-IOS
//
//  Created by Isaac on 1/25/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit

class SexPickerViewController: UIViewController, UIPickerViewDataSource {

    var pickerData=["No definido","Masculino","Femenino"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }

    
   

}
