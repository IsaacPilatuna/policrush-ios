//
//  NewCrushViewController.swift
//  PoliCrush-IOS
//
//  Created by Isaac on 1/31/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseStorage
import FirebaseAuth


class NewCrushViewController: UIViewController, UINavigationControllerDelegate,UITextFieldDelegate, UIImagePickerControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var currentTextField = UITextField()
    @IBOutlet weak var stackView: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var edadTextField: UITextField!
    @IBOutlet weak var sexTextField: UITextField!
    @IBOutlet weak var crushImage: UIImageView!
    
    @IBOutlet weak var finishButton: UIBarButtonItem!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    @IBOutlet weak var loadPhotoButton: UIButton!
    let authorUID = Auth.auth().currentUser?.uid ?? ""
    var imagePicker = UIImagePickerController()
    
    var edadesNumbers:[Int] = Array(18...60)
    var edades:[String] = []
    var sexs=["No definido","Masculino","Femenino"]
    let pickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateBackground()
        updateButtons()
        edades = edadesNumbers.map { String($0) }
        activityView.hidesWhenStopped = true
        activityView.isHidden = true

        // Do any additional setup after loading the view.
    }
    
    func startRegister(){
         activityView.isHidden = false
        activityView.startAnimating()
        finishButton.isEnabled=false
        
    }
    
    func stopRegister(){
        activityView.stopAnimating()
        finishButton.isEnabled=true
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      view.endEditing(true)
    }
    
    func updateBackground(){
              let background = CAGradientLayer()
              background.colors = [UIColor.systemPink.cgColor,UIColor.purple.cgColor]
              background.frame=view.frame
              view.layer.insertSublayer(background, at: 0)
        
        stackView.layer.backgroundColor=UIColor(white: 1, alpha: 0.8).cgColor
        stackView.layer.borderWidth=2
        stackView.layer.borderColor = UIColor.gray.cgColor
        stackView.layer.cornerRadius=10
       }
    
    func updateButtons(){
        
        loadPhotoButton.layer.cornerRadius = 10
        loadPhotoButton.layer.borderWidth=2
        loadPhotoButton.layer.borderColor=UIColor.white.cgColor
        loadPhotoButton.layer.shadowColor=UIColor.black.cgColor
        loadPhotoButton.layer.shadowOpacity=1
        loadPhotoButton.layer.shadowOffset=CGSize(width: 0.0, height: 0.2)
        
    }
    
    @IBAction func cargarFoto(_ sender: Any) {
        let actionSheet = UIAlertController(title: "Agregar Foto", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Cámara", style: .default) { (_) in
          if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = true
            
            self.present(self.imagePicker, animated: true, completion: nil)
          }
        }
        
        let libraryAction = UIAlertAction(title: "Galería", style: .default) { (_) in
          if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .savedPhotosAlbum
            self.imagePicker.allowsEditing = true
            
            self.present(self.imagePicker, animated: true, completion: nil)
          }
        }
        
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(libraryAction)
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
      crushImage.image = info[.editedImage] as! UIImage
      picker.dismiss(animated: true, completion: nil)
    }
    
    func showAlert(title: String, message: String) {
      let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
      
      let okAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
      
      alertController.addAction(okAlertAction)
      
      present(alertController, animated: true, completion: nil)
      
    }
    
    @IBAction func saveCrush(_ sender: Any) {
        startRegister()
        let uidNumber = Int64(Date().timeIntervalSince1970)
        let uid = UUID().uuidString.split(separator: "-")[0]
        let finalUID = "\(uidNumber)\(uid)"
          self.uploadImage(uid:finalUID) { imageURL in
            self.saveToFirestore(uid: finalUID,imageURL: imageURL) {
              self.showAlertSuccess(title: "Envío exitoso", message: "Su publicación se ha cargado correctamente")
                self.stopRegister()
            }
          }
        
    }
    
    func uploadImage(uid: String, completionHandler: @escaping (String) -> ()) {
      let storage = Storage.storage()
      
      let ref = storage.reference()
      let profileImages = ref.child("users")
      
      let userPicture = profileImages.child("\(uid).jpg")
      
      let data = crushImage.image?.jpegData(compressionQuality: 0.5)!
      
      let _ = userPicture.putData(data!, metadata: nil) { (metadata, error) in
        
        if error != nil {
          self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
            self.stopRegister()
        }
        
        guard let _ = metadata else {
          return
        }
        
        userPicture.downloadURL { (url, error) in
          
          if error != nil {
            self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
            self.stopRegister()
          }
          
          guard let downloadURL = url else {
            return
          }
          completionHandler(downloadURL.absoluteString)
        }
      }
    }
    
    func saveToFirestore(uid: String, imageURL: String,
                         completionHandler: @escaping ()->() ) {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "dd-MM-YYYY hh:mm"
      let fechaActualString = dateFormatter.string(from: Date())
        let firestore = Firestore.firestore()
        let document:DocumentReference = firestore.collection("crushes").document()
        document.setData([
            "autor": self.authorUID,
            "edad": Int(self.edadTextField.text ?? "0") ?? 0,
            "fecha": fechaActualString,
            "nombre": self.nameTextField.text ?? "",
            "sexo": self.sexTextField.text ?? "",
            "imageURL": "\(imageURL)",
            "votos": []
        ]){ (error) in
        
        if error != nil {
            self.stopRegister()
          self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
        }
        
        if error == nil {
          completionHandler()
        }
      }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if currentTextField==sexTextField{
            return sexs.count
        }else if currentTextField==edadTextField{
            return edades.count
        }else{
            return 0
        }
    }
    
    func showAlertSuccess(title: String, message: String) {
      let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
      
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { (alertAction) in
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }
      
      alertController.addAction(okAlertAction)
      
      present(alertController, animated: true, completion: nil)
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if currentTextField==sexTextField{
            return sexs[row]
        }else if currentTextField==edadTextField{
            return edades[row]
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if currentTextField==sexTextField{
            sexTextField.text=sexs[row]
            //self.view.endEditing(true)
        }else if currentTextField==edadTextField{
            edadTextField.text=edades[row]
            //self.view.endEditing(true)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.delegate=self
        self.pickerView.dataSource=self
        currentTextField=textField
        if(currentTextField==sexTextField || currentTextField==edadTextField){
            currentTextField.inputView=pickerView
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
