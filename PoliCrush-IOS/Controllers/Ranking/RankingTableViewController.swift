//
//  RankingTableViewController.swift
//  PoliCrush-IOS
//
//  Created by Isaac on 1/31/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseStorage
import FirebaseAuth

class RankingTableViewController: UITableViewController {
    
    let reference = Firestore.firestore().collection("crushes")
    var crushes:[Crush] = []
    let uid=Auth.auth().currentUser?.uid ?? ""
    override func viewDidLoad() {
        super.viewDidLoad()
        cargarCrushes{ numeroCrushes in
            self.tableView.reloadData()
        }
        updateBackground()
    }

    // MARK: - Table view data source
    func updateBackground(){
        let background = CAGradientLayer()
        background.colors = [UIColor.systemPink.cgColor,UIColor.purple.cgColor]
        background.frame=view.frame
      let backgroundView = UIView(frame: self.tableView.bounds)
      backgroundView.layer.insertSublayer(background, at: 0)
      self.tableView.backgroundView = backgroundView
     
      
    }
    
    
    func cargarCrushes(completionHandler: @escaping (Int)->()){
        
           self.reference.addSnapshotListener { (snapshot, error) in
               guard let snapshot = snapshot else{ return }
             var crushesList: [Crush] = []
            for document in snapshot.documents{
                   let crush = Crush(data: document.data())
                crush?.id = document.documentID
                crushesList.append(crush!)
                self.crushes = crushesList
               }
               completionHandler(snapshot.documents.count)
               
           }
       }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
       
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return crushes.count
    }
    
    func updateCellBackground(backgroundView:UIView){
        
        backgroundView.layer.backgroundColor=UIColor(white: 1, alpha: 0.8).cgColor
        backgroundView.layer.borderWidth=2
        backgroundView.layer.borderColor = UIColor.gray.cgColor
        backgroundView.layer.cornerRadius=10
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "CrushCell", for: indexPath) as! CrushTableViewCell
           let crush = crushes[indexPath.row]
           var genderImage:UIImage!
           if(crush.sexo == "Masculino"){
               cell.crushHeaderView?.layer.backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
               genderImage=UIImage(named: "male-icon.png")
           }else if (crush.sexo == "Femenino"){
               cell.crushHeaderView?.layer.backgroundColor = #colorLiteral(red: 1, green: 0.7486020753, blue: 0.8556641088, alpha: 1)
               genderImage=UIImage(named: "female-icon.png")
           }
           cell.sexButton?.setImage(genderImage, for: .normal)
           cell.nameLabel?.text = crush.nombre
           cell.ageLabel?.text = "\(crush.edad)"
        cell.documentID = crush.id
        cell.votes=crush.votos
        cell.uid=self.uid
        cell.checkVote()
        cell.votesCountLabel?.text = "\(crush.votos.count)"
        loadImageFromURl(imageView: cell.crushImageView, url: crush.imageURL)
           cell.dateLabel?.text = crush.fecha
           var backgroundView = cell.crushBackgroundView
           updateCellBackground(backgroundView: backgroundView ?? UIView())
           
           return cell
       }
    
    
    
    func loadImageFromURl(imageView:UIImageView, url: String){
        if let url = NSURL(string: url){
            if let data = NSData(contentsOf: url as URL){
                imageView.contentMode = .scaleAspectFit
                imageView.image = UIImage(data: data as Data)
            }
        }
    }
    
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
