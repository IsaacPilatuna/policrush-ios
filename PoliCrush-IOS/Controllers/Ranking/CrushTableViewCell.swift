//
//  CrushTableViewCell.swift
//  PoliCrush-IOS
//
//  Created by Isaac on 1/31/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class CrushTableViewCell: UITableViewCell {

    
    var uid:String = ""
    var documentID = ""
    var votes: [String] = []
    @IBOutlet weak var sexButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var votesCountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var voteButton: UIButton!
    @IBOutlet weak var crushImageView: UIImageView!
    @IBOutlet weak var crushBackgroundView: UIView!
    @IBOutlet weak var crushHeaderView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        checkVote()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func checkVote(){
        if votes.contains(uid){
            voteButton.setImage(UIImage(systemName: "heart.fill"), for: .normal)
        }else{
            voteButton.setImage(UIImage(systemName: "heart"), for: .normal)
        }
    }
    
    @IBAction func voteButton(_ sender: Any) {
        if votes.contains(uid){
            voteButton.setImage(UIImage(systemName: "heart"), for: .normal)
            votes.remove(at: votes.firstIndex(of: uid) ?? 0)
            votesCountLabel.text = "\(votes.count)"
            updateVote()
            
        }else{
            voteButton.setImage(UIImage(systemName: "heart.fill"), for: .normal)
            votes.append(uid)
            votesCountLabel.text = "\(votes.count)"
            updateVote()
        }
    }
    
    func updateVote(){
        let firestore = Firestore.firestore()
        firestore.collection("crushes").document(documentID).updateData(
            [
                "votos": self.votes
        ]){ (error) in
          
          if error != nil {
            self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
          }
          
          
        }
    }
    
    func showAlert(title: String, message: String) {
      let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
      
      let okAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
      
      alertController.addAction(okAlertAction)
    }
    
}
