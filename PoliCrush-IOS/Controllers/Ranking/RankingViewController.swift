//
//  RankingViewController.swift
//  PoliCrush-IOS
//
//  Created by Isaac on 1/25/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit

class RankingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        updateBackground()
        // Do any additional setup after loading the view.
    }
    

    func updateBackground(){
        let background = CAGradientLayer()
        background.colors = [UIColor.systemPink.cgColor,UIColor.purple.cgColor]
        background.frame=view.frame
        view.layer.insertSublayer(background, at: 0)
    }
}
