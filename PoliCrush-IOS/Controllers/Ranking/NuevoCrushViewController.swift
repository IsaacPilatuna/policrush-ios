//
//  NuevoCrushViewController.swift
//  PoliCrush-IOS
//
//  Created by Isaac on 1/25/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit

class NuevoCrushViewController: UIViewController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateBackground()
    }
    
    func updateBackground(){
           let background = CAGradientLayer()
           background.colors = [UIColor.systemPink.cgColor,UIColor.purple.cgColor]
           background.frame=view.frame
           view.layer.insertSublayer(background, at: 0)
    }

}
