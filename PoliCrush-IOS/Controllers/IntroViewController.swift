//
//  IntroViewController.swift
//  PoliCrush-IOS
//
//  Created by Isaac on 1/26/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController {

    @IBOutlet weak var startButton: UIButton!
    
    
    @IBOutlet weak var ownlImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let background = CAGradientLayer()
        background.colors = [UIColor.systemPink.cgColor,UIColor.purple.cgColor]
        background.frame=view.frame
        view.layer.insertSublayer(background, at: 0)
        startButton.layer.cornerRadius = 25
        startButton.layer.borderWidth=2
        startButton.layer.borderColor=UIColor.white.cgColor
        startButton.layer.shadowColor=UIColor.black.cgColor
        startButton.layer.shadowOpacity=1
        startButton.layer.shadowOffset=CGSize(width: 0.0, height: 0.2)
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
