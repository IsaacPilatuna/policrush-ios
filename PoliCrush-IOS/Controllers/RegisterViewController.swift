//
//  RegisterViewController.swift
//  PoliCrush-IOS
//
//  Created by Isaac on 1/25/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class RegisterViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var stackView: UIView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var sexTextField: UITextField!
    @IBOutlet weak var facultadTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    
    var currentTextField = UITextField()
    var sexs=["No definido","Masculino","Femenino"]
    var facultades=["Prepo","Administracion","Ciencias","Fisica","Quimica","Sistemas","Mecanica"]
    let datePicker = UIDatePicker()
    let pickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateBackground()
        updateButtons()
        prepareDatePicker()
        
        activityView.hidesWhenStopped = true
        activityView.isHidden = true
    }
    
    func updateButtons(){
        
        registerButton.layer.cornerRadius = 25
        registerButton.layer.borderWidth=2
        registerButton.layer.borderColor=UIColor.white.cgColor
        registerButton.layer.shadowColor=UIColor.black.cgColor
        registerButton.layer.shadowOpacity=1
        registerButton.layer.shadowOffset=CGSize(width: 0.0, height: 0.2)
        
    }
    
    func updateBackground(){
          let background = CAGradientLayer()
          background.colors = [UIColor.systemPink.cgColor,UIColor.purple.cgColor]
          background.frame=view.frame
          view.layer.insertSublayer(background, at: 0)
       stackView.layer.backgroundColor=UIColor(white: 1, alpha: 0.8).cgColor
       stackView.layer.borderWidth=2
       stackView.layer.borderColor = UIColor.gray.cgColor
       stackView.layer.cornerRadius=10
      }
    
    func prepareDatePicker(){
        datePicker.datePickerMode = .date
        dateTextField.delegate = self
        let dateComponent=DateComponents(year: -18)
        let maximumDate = Calendar.current.date(byAdding: dateComponent, to: Date())
        datePicker.maximumDate=maximumDate
        datePicker.addTarget(self, action: #selector(dateValueChanged(_:)), for: .valueChanged)
        dateTextField.inputView=datePicker
    }
    
    
    @objc func dateValueChanged(_ sender: UIDatePicker) {
      let format = DateFormatter()
      format.dateFormat = "dd-MM-YYYY"
      dateTextField.text = format.string(from: datePicker.date)
    }
    
      override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      view.endEditing(true)
    }
    @IBAction func registerButtonPressed(_ sender: Any) {
       
        startRegister()
        createUser{ userUID in
            self.saveToFirestore(userUID: userUID){
                self.clearFields()
                self.stopRegister()
                self.performSegue(withIdentifier: "registerSuccessSegue", sender: self)
            }
            
        }
    }
    
    
    @IBAction func togglePassword(_ sender: Any) {
        passwordTextField.isSecureTextEntry.toggle()
    }
    
    func startRegister(){
         activityView.isHidden = false
        activityView.startAnimating()
        registerButton.isEnabled=false
        
    }
    
    func clearFields(){
        usernameTextField.text=""
        emailTextField.text=""
        passwordTextField.text=""
        dateTextField.text=""
        sexTextField.text=""
        facultadTextField.text=""
    }
    
    func stopRegister(){
        activityView.stopAnimating()
        registerButton.isEnabled=true
        
    }
    
    func saveToFirestore(userUID: String,
                         completionHandler: @escaping ()->() ) {
      
      let firestore = Firestore.firestore()
        firestore.collection("users").document(userUID).setData(
            [
                "username": self.usernameTextField.text ?? "",
                "email": self.emailTextField.text ?? "",
                "birthdate": self.dateTextField.text ?? "",
                "sex": self.sexTextField.text ?? "",
                "facultad": self.facultadTextField.text ?? ""
        ]){ (error) in
          
          if error != nil {
            self.stopRegister()
            self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
          }
          
          if error == nil {
           
            completionHandler()
          }
        }
    }
    
    func createUser(completionHandler: @escaping (String)->()) {
      
      let auth = Auth.auth()
      auth.createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (authInfo, error) in
        
        if error != nil {
            self.stopRegister()
          self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
        }
        
        if error == nil {
          completionHandler(authInfo!.user.uid)
        }
      }
      
    }
    
    func showAlert(title: String, message: String) {
      let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
      
      let okAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
      
      alertController.addAction(okAlertAction)
      
      present(alertController, animated: true, completion: nil)
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if currentTextField==sexTextField{
            return sexs.count
        }else if currentTextField==facultadTextField{
            return facultades.count
        }else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if currentTextField==sexTextField{
            return sexs[row]
        }else if currentTextField==facultadTextField{
            return facultades[row]
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if currentTextField==sexTextField{
            sexTextField.text=sexs[row]
            //self.view.endEditing(true)
        }else if currentTextField==facultadTextField{
            facultadTextField.text=facultades[row]
            //self.view.endEditing(true)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.delegate=self
        self.pickerView.dataSource=self
        currentTextField=textField
        if(currentTextField==sexTextField || currentTextField==facultadTextField){
            currentTextField.inputView=pickerView
        }
        
    }
}
