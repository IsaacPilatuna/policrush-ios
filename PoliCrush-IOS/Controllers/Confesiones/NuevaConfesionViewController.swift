//
//  NuevaConfesionViewController.swift
//  PoliCrush-IOS
//
//  Created by Isaac on 1/25/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth

class NuevaConfesionViewController: UIViewController, UITextViewDelegate {

    @IBOutlet var viewBack: UIView!
    @IBOutlet weak var nuevaConfesionTextField: UITextView!
    let placeholder = "Nueva confesión de máximo 300 palabras"
    var characterCount = 0
    var fechaActualString=""
    @IBOutlet weak var sendButton: UIBarButtonItem!
    let characterCountLimit=300
    @IBOutlet weak var characterCountLabel: UILabel!
    
    @IBOutlet weak var stackView: UIView!
    
    var user = Auth.auth().currentUser
    var email = ""
    var userUID = ""
    let db = Firestore.firestore()
    var userModel:User!
    var edad=0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sendButton.isEnabled=false
        prepareTextField()
        updateBackground()
        cargarDatos()
        characterCountLabel.text="\(characterCount) / \(characterCountLimit)"
        // Do any additional setup after loading the view.
    }
    
    func prepareTextField(){
        nuevaConfesionTextField.layer.borderColor=UIColor.lightGray.cgColor
        nuevaConfesionTextField.layer.borderWidth=2
        nuevaConfesionTextField.layer.cornerRadius=10
        nuevaConfesionTextField.text=placeholder
        nuevaConfesionTextField.textColor=UIColor.gray
    }
    
    func updateBackground(){
        let background = CAGradientLayer()
        background.colors = [UIColor.systemPink.cgColor,UIColor.purple.cgColor]
        background.frame=view.frame
        view.layer.insertSublayer(background, at: 0)
        
        stackView.layer.backgroundColor=UIColor(white: 1, alpha: 0.8).cgColor
        stackView.layer.borderWidth=2
        stackView.layer.borderColor = UIColor.gray.cgColor
        stackView.layer.cornerRadius=10
    }
    
    func cargarDatos(){
        
        email=user!.email!
        userUID = user!.uid
        let docReference = db.collection("users").document(userUID)
        
        docReference.getDocument { (document, error) in
            if  error == nil{
                let data = document!.data()
                self.userModel=User(data: data)
                self.calcularEdad()
            }else{
                print("No se pudo obtener informacion de usuario \(self.userUID)")
                
            }
        }
        
    }
    
    func calcularEdad(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-YYYY"
        let fechaNacimiento = dateFormatter.date(from: self.userModel.birthDate)
        let fechaActual = Date()
        edad = Calendar.current.dateComponents([.year], from: fechaNacimiento!, to: fechaActual).year ?? 0
        
        print(edad)
        
        
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(nuevaConfesionTextField.textColor==UIColor.gray){
            nuevaConfesionTextField.text=""
            nuevaConfesionTextField.textColor=UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text.isEmpty){
            nuevaConfesionTextField.text=placeholder
            nuevaConfesionTextField.textColor=UIColor.gray
            characterCount=0
            sendButton.isEnabled=false
            updateCounterLabel()
            
        }else{
            sendButton.isEnabled=true
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if(nuevaConfesionTextField.textColor != UIColor.gray){
            characterCount=nuevaConfesionTextField.text.count
            updateCounterLabel()
        }
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        if(text=="\n"){
            textView.resignFirstResponder()
            return false
        }
        return newText.count < (characterCountLimit+1)
        
    }
    
    
    
    func updateCounterLabel(){
        characterCountLabel.text="\(characterCount) / \(characterCountLimit)"
        if(characterCount==characterCountLimit){
            characterCountLabel.textColor=UIColor.red
        }else{
            characterCountLabel.textColor=UIColor.link
        }
        
    }
    
    
    @IBAction func guardarConfesion(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-YYYY hh:mm"
        fechaActualString=dateFormatter.string(from: Date())
        let confesion = nuevaConfesionTextField.text
        let uidNumber = Int64(Date().timeIntervalSince1970)
        let uid = UUID().uuidString.split(separator: "-")[0]
        db.collection("confesiones").document("\(uidNumber)\(uid)").setData([
            "autor" : self.userUID,
            "edad" : self.edad,
            "fecha" : self.fechaActualString,
            "sexo" : self.userModel.sex,
            "confesion" : confesion ?? ""
        ]){ (error) in
            if error != nil{
                self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
                
            }else{
                self.showAlertSuccess(title: "Envío exitoso", message: "Su confesión ha sido publicada exitosamente")
            }
            
        }
    }
    
    func showAlert(title: String, message: String) {
      let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
      
      let okAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
      
      alertController.addAction(okAlertAction)
      
      present(alertController, animated: true, completion: nil)
    }
    
    func showAlertSuccess(title: String, message: String) {
      let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
      
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { (alertAction) in
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }
      
      alertController.addAction(okAlertAction)
      
      present(alertController, animated: true, completion: nil)
    }
    
}
