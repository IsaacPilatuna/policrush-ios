//
//  ConfesionesTableViewController.swift
//  PoliCrush-IOS
//
//  Created by Isaac on 1/28/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit
import FirebaseFirestore

class ConfesionesTableViewController: UITableViewController {
    
    
    let reference = Firestore.firestore().collection("confesiones")
    var confesiones:[Confesion] = []

    @IBOutlet weak var confesionBackground: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cargarConfesiones{ numeroConfesiones in
            self.tableView.reloadData()
            
        }
        updateBackground()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    
    func updateBackground(){
          let background = CAGradientLayer()
          background.colors = [UIColor.systemPink.cgColor,UIColor.purple.cgColor]
          background.frame=view.frame
        let backgroundView = UIView(frame: self.tableView.bounds)
        backgroundView.layer.insertSublayer(background, at: 0)
        self.tableView.backgroundView = backgroundView
     
        
      }
    
    func cargarConfesiones(completionHandler: @escaping (Int)->()){
        self.reference.addSnapshotListener { (snapshot, error) in
            guard let snapshot = snapshot else{ return }
            var confesionesList: [Confesion] = []
            for document in snapshot.documents{
                let confesion = Confesion(data: document.data())
                
                confesionesList.append(confesion!)
                self.confesiones = confesionesList
                
            }
            completionHandler(snapshot.documents.count)
            
        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return confesiones.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConfesionCell", for: indexPath) as! ConfesionTableViewCell
        let confesion = confesiones[indexPath.row]
        var genderImage:UIImage!
        if(confesion.sexo == "Masculino"){
            cell.headerView?.layer.backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
            genderImage=UIImage(named: "male-icon.png")
        }else if (confesion.sexo == "Femenino"){
            cell.headerView?.layer.backgroundColor = #colorLiteral(red: 1, green: 0.7486020753, blue: 0.8556641088, alpha: 1)
            genderImage=UIImage(named: "female-icon.png")
        }
        cell.genderButton?.setImage(genderImage, for: .normal)
        cell.sexLabel?.text = confesion.sexo
        cell.ageLabel?.text = "\(confesion.edad)"
        cell.confesionText?.text = confesion.confesion
        cell.dateLabel?.text = confesion.fecha
        var backgroundView = cell.confesionBackground
        updateCellBackground(backgroundView: backgroundView ?? UIView())
        
        return cell
    }
    
    func updateCellBackground(backgroundView:UIView){
        
        backgroundView.layer.backgroundColor=UIColor(white: 1, alpha: 0.8).cgColor
        backgroundView.layer.borderWidth=2
        backgroundView.layer.borderColor = UIColor.gray.cgColor
        backgroundView.layer.cornerRadius=10
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
