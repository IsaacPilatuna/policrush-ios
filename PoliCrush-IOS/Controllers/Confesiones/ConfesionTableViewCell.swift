//
//  ConfesionTableViewCell.swift
//  PoliCrush-IOS
//
//  Created by Isaac on 1/28/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit

class ConfesionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var confesionText: UITextView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var genderButton: UIButton!
    
    @IBOutlet weak var confesionBackground: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
