//
//  LoginViewController.swift
//  PoliCrush-IOS
//
//  Created by Isaac on 1/25/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    @IBOutlet weak var stackView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateButtons()
        updateBackground()

        // Do any additional setup after loading the view.
    }
    

    override func viewDidAppear(_ animated: Bool) {
      if let _ = Auth.auth().currentUser {
        performSegue(withIdentifier: "loginSuccessSegue", sender: self)
      }
    }
    
    
    func updateButtons(){
        
        registerButton.layer.cornerRadius = 25
        registerButton.layer.borderWidth=2
        registerButton.layer.borderColor=UIColor.white.cgColor
        registerButton.layer.shadowColor=UIColor.black.cgColor
        registerButton.layer.shadowOpacity=1
        registerButton.layer.shadowOffset=CGSize(width: 0.0, height: 0.2)
        
        loginButton.layer.cornerRadius = 25
        loginButton.layer.borderWidth=2
        loginButton.layer.borderColor=UIColor.white.cgColor
        loginButton.layer.shadowColor=UIColor.black.cgColor
        loginButton.layer.shadowOpacity=1
        loginButton.layer.shadowOffset=CGSize(width: 0.0, height: 0.2)
        
        forgotPasswordButton.layer.cornerRadius = 25
        forgotPasswordButton.layer.borderWidth=2
        forgotPasswordButton.layer.borderColor=UIColor.white.cgColor
        forgotPasswordButton.layer.shadowColor=UIColor.black.cgColor
        forgotPasswordButton.layer.shadowOpacity=1
        forgotPasswordButton.layer.shadowOffset=CGSize(width: 0.0, height: 0.2)    }
    
    func updateBackground(){
          let background = CAGradientLayer()
          background.colors = [UIColor.systemPink.cgColor,UIColor.purple.cgColor]
          background.frame=view.frame
          view.layer.insertSublayer(background, at: 0)
       stackView.layer.backgroundColor=UIColor(white: 1, alpha: 0.8).cgColor
       stackView.layer.borderWidth=2
       stackView.layer.borderColor = UIColor.gray.cgColor
       stackView.layer.cornerRadius=10
      }
    @IBAction func loginButtonPressed(_ sender: Any) {
        guard let email = emailTextField.text,
          let password = passwordTextField.text else {
            return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
          
          if error != nil {
            self.presentAlertWith(title: "Error", message: error?.localizedDescription ?? "Ups! An error occurred")
          } else {
            
            self.performSegue(withIdentifier: "loginSuccessSegue", sender: self)
            
          }
        }
    }
    
    private func presentAlertWith(title: String, message:String) {
      let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
      
      let okAlertAction = UIAlertAction(title: "OK", style: .default) { _ in
        self.emailTextField.text = ""
        self.passwordTextField.text = ""
        self.emailTextField.becomeFirstResponder()
      }
      
      alertController.addAction(okAlertAction)
      
      present(alertController, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    //    emailTextField.resignFirstResponder()
    //    passwordTextField.resignFirstResponder()
    //
        view.endEditing(true)
      }
    
    
    @IBAction func togglePassword(_ sender: Any) {
        passwordTextField.isSecureTextEntry.toggle()
    }
    
    @IBAction func forgotPassword(_ sender: Any) {
        let alertController = UIAlertController(title: "Recuperar contraseña", message: "Ingrese su correo para recuperar su contraseña", preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { _ in
            let recoveryEmailTextField = alertController.textFields![0] as UITextField
            if (recoveryEmailTextField.text != ""){
                Auth.auth().sendPasswordReset(withEmail: recoveryEmailTextField.text ?? "") { (error) in
                    if error != nil {
                        self.presentAlertWith(title: "Error", message:  error?.localizedDescription ?? "Ups! An error occurred")
                    }
                    if error == nil{
                        self.presentAlertWith(title: "Recuperación", message: "Los pasos para recuperar su contraseña han sido enviados a su correo")
                    }
                }
            }
            
        }
        
        let cancelAlertAction = UIAlertAction(title: "Cancelar", style: .default) { (alertAction) in }
        
        alertController.addTextField{(textfield) in
            textfield.placeholder="Correo de recuperacion"
        }
        
        alertController.addAction(okAlertAction)
        alertController.addAction(cancelAlertAction)
        
        present(alertController, animated: true, completion: nil)    }
}
