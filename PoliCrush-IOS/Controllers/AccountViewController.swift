//
//  AccountViewController.swift
//  PoliCrush-IOS
//
//  Created by Isaac on 1/23/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class AccountViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var sexTextField: UITextField!
    @IBOutlet weak var facultadTextField: UITextField!
    
    @IBOutlet weak var stackView: UIView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBOutlet weak var passwordButton: UIButton!
    
    @IBOutlet weak var logOutButton: UIButton!
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    var user = Auth.auth().currentUser
    var email = "";
    var userUID = ""
    let db = Firestore.firestore()
    var userModel:User!
    
    
    var currentTextField = UITextField()
    var sexs=["No definido","Masculino","Femenino"]
    var facultades=["Prepo","Administracion","Ciencias","Fisica","Quimica","Sistemas","Mecanica"]
    let datePicker = UIDatePicker()
    let pickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityView.hidesWhenStopped=true
        updateBackground()
        updateButtons()
        startRegister()
        cargarDatos()
        prepareDatePicker()
        

    }
    
    func updateButtons(){
        
        passwordButton.layer.cornerRadius = 25
        passwordButton.layer.borderWidth=2
        passwordButton.layer.borderColor=UIColor.white.cgColor
        passwordButton.layer.shadowColor=UIColor.black.cgColor
        passwordButton.layer.shadowOpacity=1
        passwordButton.layer.shadowOffset=CGSize(width: 0.0, height: 0.2)
        
        logOutButton.layer.cornerRadius = 25
        logOutButton.layer.borderWidth=2
        logOutButton.layer.borderColor=UIColor.white.cgColor
        logOutButton.layer.shadowColor=UIColor.black.cgColor
        logOutButton.layer.shadowOpacity=1
        logOutButton.layer.shadowOffset=CGSize(width: 0.0, height: 0.2)
    }
    
     func updateBackground(){
           let background = CAGradientLayer()
           background.colors = [UIColor.systemPink.cgColor,UIColor.purple.cgColor]
           background.frame=view.frame
           view.layer.insertSublayer(background, at: 0)
        stackView.layer.backgroundColor=UIColor(white: 1, alpha: 0.8).cgColor
        stackView.layer.borderWidth=2
        stackView.layer.borderColor = UIColor.gray.cgColor
        stackView.layer.cornerRadius=10
       }
    
    func prepareDatePicker(){
        datePicker.datePickerMode = .date
        dateTextField.delegate = self
        let dateComponent=DateComponents(year: -18)
        let maximumDate = Calendar.current.date(byAdding: dateComponent, to: Date())
        datePicker.maximumDate=maximumDate
        datePicker.addTarget(self, action: #selector(dateValueChanged(_:)), for: .valueChanged)
        dateTextField.inputView=datePicker
        
    }
    
    @objc func dateValueChanged(_ sender: UIDatePicker) {
      let format = DateFormatter()
      format.dateFormat = "dd-MM-YYYY"
      dateTextField.text = format.string(from: datePicker.date)
    }
    
      override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      view.endEditing(true)
    }
    
    func cargarDatos(){
        email=user!.email!
        emailTextField.text=email
        userUID = user!.uid
        let docReference = db.collection("users").document(userUID)
        
        docReference.getDocument { (document, error) in
            if  error == nil{
                let data = document!.data()
                self.userModel=User(data: data)
                self.usernameTextField.text=self.userModel.username
                self.dateTextField.text=self.userModel.birthDate
                self.sexTextField.text=self.userModel.sex
                self.facultadTextField.text=self.userModel.facultad
                self.stopRegister()
            }else{
                self.stopRegister()
                print("No se pudo obtener informacion de usuario \(self.userUID)")
                
            }
        }
        
    }
    

    
    @IBAction func changePassword(_ sender: Any) {
        let recoverPasswordAlert = UIAlertController(title: "Cambio de contraseña", message: "Los pasos para recuperar su contrasena serán enviados al correo \(self.email)", preferredStyle: .alert)
        recoverPasswordAlert.addAction(UIAlertAction(title: "Confirmar", style: .default, handler: { (action: UIAlertAction!) in
            Auth.auth().sendPasswordReset(withEmail: self.email) { (error) in
                                   if error != nil {
                                       self.presentAlertWith(title: "Error", message:  error?.localizedDescription ?? "Ups! An error occurred")
                                   }
                                   if error == nil{
                                       self.presentAlertWith(title: "Recuperación", message: "Los pasos para recuperar su contraseña han sido enviados a su correo")
                                   }
                               }
                
        }))
        recoverPasswordAlert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: nil))
        present(recoverPasswordAlert, animated: true, completion: nil)
    }
    
    func startRegister(){
        activityView.isHidden=false
        activityView.startAnimating()
        saveButton.isEnabled=false
    }
    
    func stopRegister(){
        activityView.stopAnimating()
        saveButton.isEnabled=true
    }
    
    @IBAction func saveChanges(_ sender: Any) {
        db.collection("users").document(userUID).setData(
            [
                "username": self.usernameTextField.text ?? "",
                "email": self.emailTextField.text ?? "",
                "birthdate": self.dateTextField.text ?? "",
                "sex": self.sexTextField.text ?? "",
                "facultad": self.facultadTextField.text ?? ""
                
        ]){ (error) in
          
          if error != nil {
            self.stopRegister()
            self.presentAlertWith(title: "Error", message: error?.localizedDescription ?? "Error")
          }else{
            self.presentAlertWith(title: "Guardado exitoso", message: "Sus datos han sido actualizados exitosamente")
            }
          
        }
    }
    
    @IBAction func logOutButton(_ sender: Any) {
        let logOutAlert = UIAlertController(title: "Cerrar Sesión", message: "¿Desea cerrar su sesión?", preferredStyle: .alert)
        logOutAlert.addAction(UIAlertAction(title: "Confirmar", style: .default, handler: { (action: UIAlertAction!) in
            do{
                try Auth.auth().signOut()
                
            }catch let signOutError as NSError{
                print("Error signing out: \(signOutError)")
            }
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }))
        logOutAlert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: nil))
        present(logOutAlert, animated: true, completion: nil)
        
    }
    
     private func presentAlertWith(title: String, message:String) {
         let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
         
         let okAlertAction = UIAlertAction(title: "OK", style: .default) { _ in
           
         }
         
         alertController.addAction(okAlertAction)
         
         present(alertController, animated: true, completion: nil)
       }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if currentTextField==sexTextField{
            return sexs.count
        }else if currentTextField==facultadTextField{
            return facultades.count
        }else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if currentTextField==sexTextField{
            return sexs[row]
        }else if currentTextField==facultadTextField{
            return facultades[row]
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if currentTextField==sexTextField{
            sexTextField.text=sexs[row]
            //self.view.endEditing(true)
        }else if currentTextField==facultadTextField{
            facultadTextField.text=facultades[row]
            //self.view.endEditing(true)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.delegate=self
        self.pickerView.dataSource=self
        currentTextField=textField
        if(currentTextField==sexTextField || currentTextField==facultadTextField){
            currentTextField.inputView=pickerView
        }
        
    }
    
}
